#!/usr/bin/env groovy

release = "v2025dev3"
branch = "apertis/"+release
credentials = "79a6e702-d0b5-423a-b8e2-8dcd0ab6d2aa"

utils_url = "git@gitlab.apertis.org:infrastructure/tiny-image-debug-utils.git"

helper_tools_url = "git@gitlab.apertis.org:tests/helper-tools.git"
helper_tools_branch = branch

node {
    stage("Checkout sources"){
      dir ("tiny-image-debug-utils") {
        git(url: utils_url,
            poll: false,
            credentialsId: credentials,
            branch: branch)
      }
    }

    stage("Checkout helper tool") {
      dir ("helper-tools") {
        git(url: helper_tools_url,
            poll: false,
            credentialsId: credentials,
            branch: helper_tools_branch)
      }
    }

    stage("Update test binaries from packages") {
      sshagent (credentials: [ credentials ] ) {
        sh "echo Current release: ${release}"
        sh "echo Current branch: ${branch}"
        sh "helper-tools/update_test_binaries.sh -r tiny-image-debug-utils -l ${release} -b ${branch}"
      }
    }

    stage("Cleanup") {
      deleteDir()
    }
}
