# Debug utilities for LXC tiny container

Additional utilities for tiny container debug and analysis.

* Create container
Create Tiny container as described in README.md file here:
https://gitlab.apertis.org/infrastructure/tiny-image-recipes/tree/master/lxc

* Download binaries to the host
with `git clone https://gitlab.apertis.org/infrastructure/tiny-image-debug-utils`
or with downloading and unpacking of the tarball created from GitLab repository
https://gitlab.apertis.org/infrastructure/tiny-image-debug-utils

* Add mount entry into `config` file of container
```
lxc.mount.entry = /home/user/tiny-image-debug-utils/amd64 var/utils none bind,create=dir 0 0
```
there `/home/user/tiny-image-debug-utils/amd64` is a path to directory with binaries for host's
architecture prepared on previous step

* For utility `top` and others which using terminfo -- need a terminfo DB inside of container,
so additional entry is needed:
```
lxc.mount.entry = /lib/terminfo lib/terminfo none bind,ro,create=dir 0 0
```

* Start the container

* Attach to container
Need to re-define shell variables PATH and LD_LIBRARY_PATH to work with prepared binaries:
```
lxc-attach -v LD_LIBRARY_PATH=/var/utils/lib -v PATH=/var/utils/bin:$PATH -n tinyrfs bash
```
this command gives a `bash` shell to the container allowing to use additional binaries.
